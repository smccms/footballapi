﻿using System.Net.Http;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using FootballAPI.Controllers;
using FootballAPI.Models;
using System.Threading.Tasks;

namespace FootballAPI.Tests.Controllers
{
    [TestClass]
    public class FootballControllerTest
    {
        // GET /api/teams
        [TestMethod]
        public void Get()
        {
            // Arrange
            TeamsController controller = new TeamsController();

            // Act
            HttpResponseMessage result = controller.Get();
            List<Team> teams = result.Content.ReadAsAsync<List<Team>>().Result;

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(2, teams.Count);

            Team team1 = teams[0];                       
            Assert.AreEqual("team1", team1.Name);
            Assert.AreEqual("coach1", team1.Coach);
            Assert.AreEqual(0, team1.NumberOfMatches);
            Assert.AreEqual(0, team1.NumberOfMatchesWonLost);

            Team team2 = teams[1];
            Assert.AreEqual("team2", team2.Name);
            Assert.AreEqual("coach2", team2.Coach);
            Assert.AreEqual(0, team2.NumberOfMatches);
            Assert.AreEqual(0, team2.NumberOfMatchesWonLost);
        }

        // GET /api/teams/{name}
        [TestMethod]
        public void GetByName()
        {
            // Arrange
            TeamsController controller = new TeamsController();

            // Act
            HttpResponseMessage result = controller.Get("team1");
            List<Team> team = result.Content.ReadAsAsync<List<Team>>().Result;

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(1, team.Count);

            Assert.AreEqual("team1", team[0].Name);
            Assert.AreEqual("coach1", team[0].Coach);
            Assert.AreEqual(0, team[0].NumberOfMatches);
            Assert.AreEqual(0, team[0].NumberOfMatchesWonLost);
        }
        // GET /api/teams/coach/{coach}
        [TestMethod]
        public void GetByCoachList()
        {
            // Arrange
            TeamsController controller = new TeamsController();

            // Act
            HttpResponseMessage result = controller.GetFromCoach("coach");
            List<Team> teams = result.Content.ReadAsAsync<List<Team>>().Result;

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(2, teams.Count);

            Team team1 = teams[0];
            Assert.AreEqual("team1", team1.Name);
            Assert.AreEqual("coach1", team1.Coach);
            Assert.AreEqual(0, team1.NumberOfMatches);
            Assert.AreEqual(0, team1.NumberOfMatchesWonLost);

            Team team2 = teams[1];
            Assert.AreEqual("team2", team2.Name);
            Assert.AreEqual("coach2", team2.Coach);
            Assert.AreEqual(0, team2.NumberOfMatches);
            Assert.AreEqual(0, team2.NumberOfMatchesWonLost);
        }

        [TestMethod]
        public void GetByCoachItem()
        {
            // Arrange
            TeamsController controller = new TeamsController();

            // Act
            HttpResponseMessage result = controller.GetFromCoach("coach1");
            List<Team> teams = result.Content.ReadAsAsync<List<Team>>().Result;

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(1, teams.Count);

            Team team1 = teams[0];
            Assert.AreEqual("team1", team1.Name);
            Assert.AreEqual("coach1", team1.Coach);
            Assert.AreEqual(0, team1.NumberOfMatches);
            Assert.AreEqual(0, team1.NumberOfMatchesWonLost);
        }
        // GET /api/teams/{id}
        [TestMethod]
        public void GetById()
        {
            // Arrange
            TeamsController controller = new TeamsController();

            // Act
            HttpResponseMessage result = controller.Get(0);
            Team team = result.Content.ReadAsAsync<Team>().Result;

            // Assert
            Assert.IsNotNull(result);
            
            Assert.AreEqual("team1", team.Name);
            Assert.AreEqual("coach1", team.Coach);
            Assert.AreEqual(0, team.NumberOfMatches);
            Assert.AreEqual(0, team.NumberOfMatchesWonLost);
        }

        // DELETE /api/teams/{id}
        [TestMethod]
        public void DeleteById()
        {
            // Arrange
            TeamsController controller = new TeamsController();

            // Act
            HttpResponseMessage result = controller.Delete(0);
            HttpResponseMessage result_2 = controller.Get();
            List<Team> team = result_2.Content.ReadAsAsync<List<Team>>().Result;
            
            // Assert
            Assert.IsNotNull(result);
            Assert.IsNotNull(result_2);
            Assert.AreEqual(1, team.Count);
            
        }
    }
}
