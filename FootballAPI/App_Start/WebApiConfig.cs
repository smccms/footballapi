﻿using FootballAPI.Log;
using System;
using System.Web.Http;

namespace FootballAPI
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            GlobalConfiguration.Configuration.MessageHandlers.Add(new MessageLoggingHandler());
            // Web API routes
            config.MapHttpAttributeRoutes();
        }
    }
}
