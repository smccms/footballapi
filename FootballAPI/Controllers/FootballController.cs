﻿using FootballAPI.Models;
using FootballAPI.Repository;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace FootballAPI.Controllers
{
    [RoutePrefix("api/teams")]
    public class TeamsController : ApiController
    {
        private TeamsRepository teamsRepository;

        public TeamsController()
        {
            this.teamsRepository = new TeamsRepository();
            this.Request = new HttpRequestMessage();
            this.Request.SetConfiguration(new HttpConfiguration());
        }

        // Retrieve all teams names and ids;
        // GET /api/teams
        [Route]
        public HttpResponseMessage Get()
        {
            List<Team> teams = teamsRepository.GetAllTeams();
            if(teams == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, teams);
            }
            return Request.CreateResponse(HttpStatusCode.OK, teams);
        }

        // Search teams by name;
        // GET /api/teams/{name}
        [Route("{name}")]
        public HttpResponseMessage Get(string name)
        {
            List<Team> teams = teamsRepository.GetTeamsByName(name);
            if (teams == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }
            return Request.CreateResponse(HttpStatusCode.OK, teams);
        }

        // Search teams by coach name;
        // GET /api/teams/coach/{coach}
        [Route("coach/{coach}")]
        public HttpResponseMessage GetFromCoach(string coach)
        {
            List<Team> teams = teamsRepository.GetTeamsByCoach(coach);
            if (teams == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }
            return Request.CreateResponse(HttpStatusCode.OK, teams);
        }

        // Retrieve the details of a specific team.
        // GET /api/teams/{id}
        [Route("{id:int}")]
        public HttpResponseMessage Get(long id)
        {
            Team team = teamsRepository.GetTeam(id);
            if (team == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }
            return Request.CreateResponse(HttpStatusCode.OK, team);
        }

        // Add team;
        // POST /api/teams
        [Route]
        public async Task<HttpResponseMessage> PostAsync()
        {
            Team team = await Request.Content.ReadAsAsync<Team>();
            teamsRepository.Add(team);
            return Request.CreateResponse(HttpStatusCode.Created);
        }

        // Add/Delete teams;
        // DELETE /api/teams/{id}
        [Route("{id:int}")]
        public HttpResponseMessage Delete(long id)
        {
            teamsRepository.Delete(id);
            return Request.CreateResponse(HttpStatusCode.OK);
        }

        // Update the teams information;
        // PUT /api/teams/{id}
        [Route("{id:int}")]
        public async Task<HttpResponseMessage> PutAsync(long id)
        {
            Team team = await Request.Content.ReadAsAsync<Team>();
            teamsRepository.UpdateTeam(id, team);
            return Request.CreateResponse(HttpStatusCode.OK);
        }

        // Update lost/won matches;
        // PUT /api/teams/{id}/lostwin
        [Route("{id:int}/lostwon")]
        public async Task<HttpResponseMessage> PutLostWonAsync(long id)
        {
            JObject body = await Request.Content.ReadAsAsync<JObject>();
            int lostWon = body.GetValue("lostwon").Value<int>();
            teamsRepository.UpdateLostWon(id, lostWon);
            return Request.CreateResponse(HttpStatusCode.OK);
        }
        
        
    }
}
