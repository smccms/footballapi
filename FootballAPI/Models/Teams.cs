﻿using System;

namespace FootballAPI.Models
{
    public class Team
    {
        public long Id { get; set; }
        public String Name { get; set; }
        public String Coach { get; set; }
        public long NumberOfMatches { get; set; }
        public long NumberOfMatchesWonLost { get; set; }
    }
}