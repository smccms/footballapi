﻿using Microsoft.EntityFrameworkCore;

namespace FootballAPI.Models
{
    public class FootballContext : DbContext
    {
        public FootballContext(DbContextOptions<FootballContext> options) 
            : base(options)
        {
        }
        public DbSet<Team> Teams { get; set; }
    }
} 