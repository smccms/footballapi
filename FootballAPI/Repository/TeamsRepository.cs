﻿using System;
using System.Collections.Generic;
using System.Linq;
using FootballAPI.Models;

namespace FootballAPI.Repository
{
    public class TeamsRepository
    {
        List<Team> teams = new List<Team>();

        public TeamsRepository()
        {
            Team team1 = new Team() { Id = 0,  Name = "team1", Coach = "coach1", NumberOfMatches = 0, NumberOfMatchesWonLost = 0 };
            Team team2 = new Team() { Id = 1, Name = "team2", Coach = "coach2", NumberOfMatches = 0, NumberOfMatchesWonLost = 0 };
            teams.Add(team1);
            teams.Add(team2);
        }

        public List<Team> GetAllTeams()
        {
            return teams;
        }

        public Team GetTeam(long id)
        {
            int idx = teams.FindIndex(x => x.Id == id);

            return teams.ElementAt(idx);
        }

        public void Add(Team team)
        {
            teams.Add(team);
        }

        public void Delete(long id)
        {
            int idx = teams.FindIndex(x => x.Id == id);
            teams.RemoveAt(idx);
        }

        internal List<Team> GetTeamsByName(string name)
        {
            return teams.FindAll(team => team.Name.Contains(name));
        }

        internal List<Team> GetTeamsByCoach(string coach)
        {
            return teams.FindAll(team => team.Coach.Contains(coach));
        }

        internal void UpdateLostWon(long id, int lostWon)
        {
            int idx = teams.FindIndex(x => x.Id == id);

            teams[idx].NumberOfMatchesWonLost = lostWon;
        }

        internal void UpdateTeam(long id, Team team)
        {
            int idx = teams.FindIndex(x => x.Id == id);
            teams[idx].Name = team.Name;
            teams[idx].Coach = team.Coach;
            teams[idx].NumberOfMatches = team.NumberOfMatches;
            teams[idx].NumberOfMatchesWonLost = team.NumberOfMatches;
        }
    }
}